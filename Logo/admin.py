from django.contrib import admin

from Logo.models import *


class LogoSizeAdmin(admin.ModelAdmin):
    list_display = ('size', 'status_id')


class LogoImageAdmin(admin.ModelAdmin):
    list_display = ('image', 'status_id')


class LogoCreationInfoAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'address', 'business_name', 'status_id',)


admin.site.register(LogoSize, LogoSizeAdmin)
admin.site.register(LogoImage, LogoImageAdmin)
admin.site.register(LogoCreationInfo, LogoCreationInfoAdmin)
