from Account.models import Role
from Account.serializers import RoleSerializer
from Logo.models import LogoSize
from Logo.serializers import LogoSizeSerializer
from ResponseHandle import exception_handler
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated


@api_view(['GET'])
def get_logo_sizes(request):
    obj = LogoSize.objects.all().values()
    return exception_handler.error_handling(data=obj, status_code=200)


@api_view(['GET'])
# @permission_classes([IsAuthenticated])
def get_logo_size(request, logo_id):
    try:
        obj = LogoSize.objects.get(id=logo_id)
    except LogoSize.DoesNotExist:
        exception_handler.error_handling(errMsg='id does not exist', )
    serializer = LogoSizeSerializer(obj)
    return exception_handler.error_handling(data=serializer.data, status_code=200)


@api_view(['PUT'])
# @permission_classes([IsAuthenticated])
def update_logo_size(request, logo_id):
    try:
        obj = LogoSize.objects.get(id=logo_id)
    except LogoSize.DoesNotExist:
        return exception_handler.error_handling(errMsg='id does not exist', )

    serializer = LogoSizeSerializer(obj, data=request.data)
    if serializer.is_valid():
        serializer.save()
        return exception_handler.error_handling(data=serializer.data, status_code=200)
    return exception_handler.error_handling(serializer.errors)
    # return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
# @permission_classes([IsAuthenticated])
def save_logo_size(request):
    serializer = LogoSizeSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return exception_handler.error_handling(data=serializer.data)
    return exception_handler.error_handling(errMsg=serializer.errors)


@api_view(['DELETE'])
# @permission_classes([IsAuthenticated])
def delete_logo_size(request, logo_id):
    try:
        role = LogoSize.objects.get(id=logo_id)
    except LogoSize.DoesNotExist:
        exception_handler.error_handling(errMsg='id does not exist', )
    role.delete()
    return exception_handler.error_handling(data='role id delete succefully', status_code=200)
