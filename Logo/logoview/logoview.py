from Account.models import Role
from Account.serializers import RoleSerializer
from Logo.models import LogoImage, LogoCreationInfo
from Logo.serializers import LogoImageSerializer, LogoCreationInfoSerializer
from ResponseHandle import exception_handler
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated


@api_view(['GET'])
def get_logo_creations(request):
    obj = LogoCreationInfo.objects.all().values(
    )
    return exception_handler.error_handling(data=obj, status_code=200)


@api_view(['GET'])
# @permission_classes([IsAuthenticated])
def get_logo_create(request, logo_id):
    try:
        obj = LogoCreationInfo.objects.get(id=logo_id)
    except LogoCreationInfo.DoesNotExist:
        exception_handler.error_handling(errMsg='id does not exist', )
    serializer = LogoCreationInfoSerializer(obj)
    return exception_handler.error_handling(data=serializer.data, status_code=200)


@api_view(['PUT'])
# @permission_classes([IsAuthenticated])
def update_logo_create(request, logo_id):
    try:
        obj = LogoCreationInfo.objects.get(id=logo_id)
    except LogoCreationInfo.DoesNotExist:
        return exception_handler.error_handling(errMsg='id does not exist', )

    serializer = LogoCreationInfoSerializer(obj, data=request.data)
    if serializer.is_valid():
        serializer.save()
        return exception_handler.error_handling(data=serializer.data, status_code=200)
    return exception_handler.error_handling(serializer.errors)
    # return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
# @permission_classes([IsAuthenticated])
def logo_create_info(request):
    serializer = LogoCreationInfoSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return exception_handler.error_handling(data=serializer.data)
    return exception_handler.error_handling(errMsg=serializer.errors)


@api_view(['DELETE'])
# @permission_classes([IsAuthenticated])
def delete_logo_image(request, logo_id):
    try:
        logo = LogoImage.objects.get(id=logo_id)
    except LogoImage.DoesNotExist:
        exception_handler.error_handling(errMsg='id does not exist', )
    logo.delete()
    return exception_handler.error_handling(data='role id delete succefully', status_code=200)
