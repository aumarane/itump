from django.db import models

from Account.models import Status
from Master.models import CountryMaster, State
from Order.models import OrderDetails


class LogoSize(models.Model):
    size = models.TextField()
    status_id = models.ForeignKey(Status, on_delete=models.CASCADE, default=1)


class LogoImage(models.Model):
    image = models.ImageField()
    status_id = models.ForeignKey(Status, on_delete=models.CASCADE, default=1)


class LogoCreationInfo(models.Model):
    first_name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250)
    address = models.CharField(max_length=250)
    address_line_two = models.CharField(max_length=250)
    business_name = models.CharField(max_length=250)
    business_address = models.CharField(max_length=250)
    primary_business_color = models.CharField(max_length=250)
    description = models.CharField(max_length=250)
    country_id = models.ForeignKey(CountryMaster, on_delete=models.CASCADE)
    state_id = models.ForeignKey(State, on_delete=models.CASCADE)
    city = models.CharField(max_length=250, blank=True, null=True)
    zip_code = models.CharField(max_length=250)
    logo_image = models.ImageField(upload_to='logoimage', blank=True, null=True)
    order_id = models.ForeignKey(OrderDetails, on_delete=models.CASCADE, related_name='logo')
    logo_image_id = models.ForeignKey(LogoImage, on_delete=models.CASCADE)
    status_id = models.ForeignKey(Status, on_delete=models.CASCADE, default=1)



