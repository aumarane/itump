from django.urls import path

from Logo.logoview import logoview, logosize, logoimage

urlpatterns = [
    # path('admin/', admin.site.urls),
    path('logo_create_info', logoview.logo_create_info),
    path('get_logo_creations', logoview.get_logo_creations),
    path('get_logo_create/<int:logo_id>', logoview.get_logo_create),
    path('update_logo_create/<int:logo_id>', logoview.update_logo_create),

    # logo size
    path('get_logo_sizes', logosize.get_logo_sizes),
    path('get_logo_size/<int:logo_id>', logosize.get_logo_size),

    # get_logo_images
    path('get_logo_images', logoimage.get_logo_images),
    path('get_logo_image/<int:logo_id>', logoimage.get_logo_image),



]
