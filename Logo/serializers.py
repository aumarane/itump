from Account.models import Role, UserModule
from rest_framework import serializers

from .models import *


class LogoSizeSerializer(serializers.ModelSerializer):
    class Meta:
        model = LogoSize
        fields = '__all__'

    def create(self, validated_data):
        return LogoSize.objects.create(**validated_data)


class LogoImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = LogoImage
        fields = '__all__'


class LogoCreationInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = LogoCreationInfo
        fields = '__all__'

    def create(self, validated_data):
        return LogoCreationInfo.objects.create(**validated_data)