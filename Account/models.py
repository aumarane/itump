from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)

class Status(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name


class Role(models.Model):
    role_name = models.CharField(max_length=250)
    status_id = models.ForeignKey(Status, on_delete=models.CASCADE, default=1)


class MyUserManager(BaseUserManager):

    def create_user(self,  name,  email, phone, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            name=name,
            phone=phone,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, name, phone, password=None):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        # try:
        #     roll_detail = RollTypeMaster.objects.get(id=roll_id)
        # except RollTypeMaster.DoesNotExist:
        #     raise ValueError('roll id does not exist')

        user = self.create_user(
            email=email,
            sur_name=sur_name,
            first_name=first_name,
            last_name=last_name,
            mobile_number=mobile_number,
            bio=bio,
            roll_id=roll_detail,
            password=password,
            # branch_id = branch_id
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class UserModule(AbstractBaseUser):
    email = models.EmailField(unique=True)
    phone = models.CharField(max_length=250)
    name = models.CharField(max_length=250)
    status_id = models.ForeignKey(Status, on_delete=models.CASCADE)
    is_admin = models.BooleanField(default=False)

    objects = MyUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name','phone',]

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin



class UserRoleMapping(models.Model):
    user_id = models.ForeignKey(UserModule, on_delete=models.CASCADE)
    role_id = models.ForeignKey(Role, on_delete=models.CASCADE)


class Profile(models.Model):
    user_id = models.ForeignKey(UserModule, on_delete=models.CASCADE)