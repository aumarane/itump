from rest_framework.decorators import api_view

from Account.models import UserModule
from Account.serializers import UserModuleSerializer, UserRoleMappingSerializer
from ResponseHandle import exception_handler


@api_view(['POST'])
def signup_create(request):
    if not request.data:
        return exception_handler.error_handling(errMsg="invalid access")
    email = UserModule.objects.filter(email=request.data['email'])

    if len(email) != 0:
        return exception_handler.error_handling(errMsg="Email id is already register.")

    serializer = UserModuleSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()

        # get update to role mapping
        obj = {
            "user_id": serializer.data['id'],
            "role_id": request.data['role_id']
        }
        user_role = UserRoleMappingSerializer(data=obj)
        if user_role.is_valid():
            user_role.save()
        else:
            return exception_handler.error_handling(user_role.errors)

        output = dict(id=serializer.data['id'], full_name=serializer.data['name'],
                      email=serializer.data['email'], remarks='register successfully user')
        return exception_handler.error_handling(data=output)
    return exception_handler.error_handling(errMsg='invalid access', array_error=serializer.errors)
