from django.contrib.auth import authenticate
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import update_last_login
from rest_framework.decorators import api_view
from rest_framework_simplejwt.tokens import RefreshToken

from Account.models import UserModule
from ResponseHandle import exception_handler

from rest_framework_jwt.settings import api_settings

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

@api_view(['POST'])
def user_login1(request):
    email = request.data['email']
    password = request.data['password']

    try:
        myuser = UserModule.objects.get(email__exact=email)
    except UserModule.DoesNotExist:
        output = "email is not registered......"
        return exception_handler.error_handling(errMsg=output)

    if not check_password(password, myuser.password):
        output = "password  is wrong"
        return exception_handler.error_handling(errMsg=output)

    user = authenticate(email=email, password=password)
    if user:
        update_last_login(None, user)
        # update_last_login(None, user)
        # payload = JWT_PAYLOAD_HANDLER(user)
        # token = JWT_ENCODE_HANDLER(payload)
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)

        # refresh = RefreshToken.for_user(user)
        output = {
                    'token': token,
                    # 'access': str(refresh.access_token),

                }

        return exception_handler.error_handling(data=output, status_code=201)
    else:
        output = 'can not authenticate with the given credentials or the account has been deactivated'
        return exception_handler.error_handling(errMsg=output)
