from django.contrib.auth.hashers import check_password
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated

from Account.models import User
from ResponseHandle import exception_handler

from Account.serializers import UserProfileSerializer


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_profile(request):
    data = {
        "id": request.user.id,
        "first_name": request.user.first_name,
        "last_name": request.user.last_name,
        "email": request.user.email,
        "mobile_number": request.user.mobile_number,
        "status": request.user.is_active,
        "created_time_stamp": request.user.created_time_stamp,
        "role_id": request.user.role_id.id,
        "role_type": request.user.role_id.role_type
    }
    print(data)
    return exception_handler.error_handling(data=data, status_code=200)


@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def update_profile(request):
    if not request.data:
        return exception_handler.error_handling('invalid access')

    serializer = UserProfileSerializer(request.user, data=request.data)
    if serializer.is_valid():
        serializer.save()
        return exception_handler.error_handling(data=serializer.data)
    return exception_handler.error_handling(data=serializer.errors)


@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def change_password(request):

    if not request.data:
        return exception_handler.error_handling(errMsg="invalid access", )

    user_id = request.user.id

    old_password = request.data['old_password']
    new_password = request.data['new_password']

    if not old_password:
        return exception_handler.error_handling(errMsg="old password is empty")

    if not new_password:
        return exception_handler.error_handling(errMsg="new password is empty")

    try:
        user = User.objects.get(id=user_id)
    except User.DoesNotExist:
        return exception_handler.error_handling(errMsg="This id does not exist")

    if not check_password(old_password, user.password):
        output = "Password is wrong!"
        return exception_handler.error_handling(errMsg=output)

    user.set_password(new_password)
    user.save()
    output = {"change_password": "password changed succefully"}
    return exception_handler.error_handling(data=output, status_code=201)

