from Account.models import Role
from Account.serializers import RoleSerializer
from ResponseHandle import exception_handler
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated


@api_view(['GET'])
def get_roles(request):
    role = Role.objects.all().values(
        'id',
        'role_name'
    )
    return exception_handler.error_handling(data=role, status_code=200)


@api_view(['GET'])
# @permission_classes([IsAuthenticated])
def get_role(request, role_id):
    try:
        role = Role.objects.get(id=role_id)
    except Role.DoesNotExist:
        exception_handler.error_handling(errMsg='role id does not exist', )
    serializer = RoleSerializer(role)
    return exception_handler.error_handling(data=serializer.data, status_code=200)


@api_view(['PUT'])
# @permission_classes([IsAuthenticated])
def update_role(request, role_id):
    try:
        role_obj = Role.objects.get(id=role_id)
    except Role.DoesNotExist:
        return exception_handler.error_handling(errMsg='role id does not exist', )

    serializer = RoleSerializer(role_obj, data=request.data)
    if serializer.is_valid():
        serializer.save()
        return exception_handler.error_handling(data=serializer.data, status_code=200)
    return exception_handler.error_handling(serializer.errors)
    # return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
# @permission_classes([IsAuthenticated])
def save_role(request):
    serializer = RoleSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return exception_handler.error_handling(data=serializer.data)
    return exception_handler.error_handling(errMsg=serializer.errors)


@api_view(['DELETE'])
# @permission_classes([IsAuthenticated])
def delete_role(request, role_id):
    try:
        role = Role.objects.get(id=role_id)
    except Role.DoesNotExist:
        exception_handler.error_handling(errMsg='role id does not exist', )
    role.delete()
    return exception_handler.error_handling(data='role id delete succefully', status_code=200)
