from django.urls import path

from Account.accview import roleview, signup, login

urlpatterns = [
    # path('admin/', admin.site.urls),
    # get roles
    path('get_roles', roleview.get_roles),
    path('get_role/<int:role_id>', roleview.get_role),
    path('update_role/<int:role_id>', roleview.update_role),
    path('delete_role/<int:role_id>', roleview.delete_role),
    path('save_role', roleview.save_role),

    path('user_login', login.user_login1),
    path('signup_create', signup.signup_create),
]
