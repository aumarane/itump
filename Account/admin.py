from django.contrib import admin

from Account.models import *


class StatusAdmin(admin.ModelAdmin):
    list_display = ('name',)


class RoleAdmin(admin.ModelAdmin):
    list_display = ('role_name','status_id',)


class UserAdmin(admin.ModelAdmin):
    list_display = ('email','phone','name','status_id',)


class ProfileAdmin(admin.ModelAdmin):
    fields = ('user_id',)


admin.site.register(Status, StatusAdmin)
admin.site.register(Role, RoleAdmin)
admin.site.register(UserModule, UserAdmin)
admin.site.register(Profile, ProfileAdmin)
