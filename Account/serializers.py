from Account.models import Role, UserModule, UserRoleMapping
from rest_framework import serializers


class RoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = '__all__'

    def create(self, validated_data):
        return Role.objects.create(**validated_data)




class UserRoleMappingSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserRoleMapping
        fields = '__all__'

    def create(self, validated_data):
        return UserRoleMapping.objects.create(**validated_data)


class UserModuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserModule
        fields = '__all__'

    def create(self, validated_data):
        user = UserModule(
            name=validated_data['name'],
            email=validated_data['email'],
            phone=validated_data['phone'],
            status_id=validated_data['status_id'],
        )
        user.set_password(validated_data['password'])
        user.save()
        return user


class LoginsSerializer(serializers.ModelSerializer):
    # role_type = serializers.CharField(source='roll_id.user_type')
    # full_name = serializers.SerializerMethodField()

    class Meta:
        model = UserModule
        fields = [
            'email',
            'name',
            'phone',
            'id',
        ]
        read_only_fields = ('password',)

    # def get_full_name(self, obj):
    #     return '{} {}'.format(obj.first_name, obj.last_name)


# # update the user profile serializer
# class UserProfileSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = User
#         fields = ('first_name',
#                   'last_name',
#                   'mobile_number'
#                   )
#
