from .serializers import LoginsSerializer


def my_jwt_response_handler(user=None, request=None):
    return {
        'user': LoginsSerializer(user).data
    }


def jwt_get_username_from_payload_handler(user):
    return user['user']['email']
