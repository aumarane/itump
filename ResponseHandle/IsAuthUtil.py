# check the validations is auth
from datetime import datetime


def AuthValidation(user_id):
    output = {
        "created_by": user_id,
        "update_by": user_id
    }
    return output


def update_user(user_id):
    output = {
        "update_by": user_id,
        "update_date_time": datetime.now()
    }
    return output
