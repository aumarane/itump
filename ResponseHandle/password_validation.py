import re
def password_check(passwd):
   val = None
   if len(passwd) < 8:
      val = 'length should be at least 8'
      return val
   if len(passwd) > 16:
      val = 'length should be not be greater than 16'
      return val

   if not any(char.isdigit() for char in passwd):
      val = 'Password should have at least one numeral'
      return val

   if not any(char.isupper() for char in passwd):
      val = 'Password should have at least one uppercase letter'
      return val
   if not any(char.islower() for char in passwd):
      val = 'Password should have at least one lowercase letter'

   if not any(char.isalpha() for char in passwd):
      val = 'Password should have at least one alpha letter'

   x = re.findall(
       r'(abc|bcd|cde|def|efg|fgh|ghi|hij|ijk|jkl|klm|lmn|mno|nop|opq|pqr|qrs|rst|stu|tuv|uvw|vwx|wxy|xyz|012|123|234|345|456|567|678|789)+',passwd)
   if len(x)>0:
      val = 'password should not have 3 or more sequential characters or number'

   letter_same = re.findall(r'[a-z]{3}', passwd)

   if len(letter_same)>0:
      val = 'matching letters in the same order as their name'

   return val
