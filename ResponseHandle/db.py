
from django.db import connection

def search_query(sql, array):
    with connection.cursor() as cursor:
        cursor.execute(
            sql,array
        )
        query = []
        for i in cursor.description:
            columns = [col[0] for col in cursor.description]
            data = [
                dict(zip(columns, row))

                for row in cursor.fetchall()
            ]
            query.append(data)
    return query[0]
