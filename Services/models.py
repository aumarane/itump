from django.db import models

from Account.models import Status


class Services(models.Model):
    name = models.CharField(max_length=250)
    cost = models.FloatField()
    description = models.TextField()
    status_id = models.ForeignKey(Status, on_delete=models.CASCADE, default=1)


class ServicesAddon(models.Model):
    name = models.CharField(max_length=250)
    cost = models.FloatField()
    description = models.TextField()
    status_id = models.ForeignKey(Status, on_delete=models.CASCADE, default=1)


class ServicesAddonMapping(models.Model):
    services_id = models.ForeignKey(Services, on_delete=models.CASCADE)
    addon_id = models.ForeignKey(ServicesAddon, on_delete=models.CASCADE)
