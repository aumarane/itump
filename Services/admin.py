from django.contrib import admin

from Services.models import *

admin.site.register(Services)
admin.site.register(ServicesAddon)
admin.site.register(ServicesAddonMapping)
