from Account.models import Role, UserModule
from rest_framework import serializers

from .models import *


class CountryMasterSerializer(serializers.ModelSerializer):
    class Meta:
        model = CountryMaster
        fields = '__all__'

    def create(self, validated_data):
        return CountryMaster.objects.create(**validated_data)


class StateModuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = State
        fields = '__all__'
