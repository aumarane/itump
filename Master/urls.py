from django.urls import path

from Master.MasterView import country, state

urlpatterns = [
# url country
    path('get_countryies', country.get_countryies),
    path('get_country/<int:country_id>', country.get_country),
    path('update_country/<int:country_id>', country.update_country),
    path('delete_country/<int:role_id>', country.delet_country),
    path('save_country', country.save_country),

    # url state
    path('get_states', state.get_states),
    path('get_state/<int:state_id>', state.get_state),
    path('update_state/<int:state_id>', state.update_state),
    path('delete_state/<int:state_id>', state.delete_state),
    path('save_state', state.save_state),

]
