from rest_framework.decorators import api_view, permission_classes

from Master.models import CountryMaster
from Master.serializers import *
from ResponseHandle import exception_handler
from rest_framework.permissions import IsAuthenticated
from ResponseHandle.IsAuthUtil import AuthValidation


@api_view(['GET'])
def get_countryies(request):
    role = CountryMaster.objects.all().values()
    return exception_handler.error_handling(data=role, status_code=200)

@api_view(['GET'])
# @permission_classes([IsAuthenticated])
def get_country(request, country_id):

    try:
        cntry = CountryMaster.objects.get(id=country_id)

    except CountryMaster.DoesNotExist:
        exception_handler.error_handling(errMsg='country id id does not exist',)

    serializer = CountryMasterSerializer(cntry)
    return exception_handler.error_handling(data=serializer.data, status_code=200)


@api_view(['PUT'])
# @permission_classes([IsAuthenticated])
def update_country(request, country_id):
    try:
        cntry = CountryMaster.objects.get(id=country_id)
    except CountryMaster.DoesNotExist:
        exception_handler.error_handling(errMsg='country id does not exist',)
    serializer = CountryMasterSerializer(cntry, data=request.data, partial=True)
    if serializer.is_valid():
        serializer.save()
        return exception_handler.error_handling(data=serializer.data)
    return exception_handler.error_handling(serializer.errors)



@api_view(['POST'])
# @permission_classes([IsAuthenticated])
def save_country(request):
    # add to create by and update by data
    auth_detail = AuthValidation(user_id=request.user.id)
    request.data.update(auth_detail)

    serializer = CountryMasterSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return exception_handler.error_handling(data=serializer.data)
    return exception_handler.error_handling(errMsg=serializer.errors)

@api_view(['DELETE'])
# @permission_classes([IsAuthenticated])
def delet_country(request, role_id):
    try:
        cntr = Country.objects.get(id=role_id)
    except Country.DoesNotExist:
        exception_handler.error_handling(errMsg='role id does not exist',)
    cntr.delete()
    return exception_handler.error_handling(data='role id delete succefully', status_code=200)



