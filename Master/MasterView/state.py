from Master.models import State
from Master.serializers import *
from ResponseHandle import exception_handler
from ResponseHandle.IsAuthUtil import AuthValidation
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated


@api_view(['GET'])
def get_states(request):
    role = State.objects.all().values()
    return exception_handler.error_handling(data=role, status_code=200)


@api_view(['GET'])
# @permission_classes([IsAuthenticated])
def get_state(request, state_id):
    try:
        role = State.objects.get(id=state_id)
    except State.DoesNotExist:
        exception_handler.error_handling(errMsg='role id does not exist', )
    serializer = StateModuleSerializer(role)
    return exception_handler.error_handling(data=serializer.data, status_code=200)


@api_view(['PUT'])
# @permission_classes([IsAuthenticated])
def update_state(request, state_id):
    try:
        role = State.objects.get(id=state_id)
    except State.DoesNotExist:
        exception_handler.error_handling(errMsg='role id does not exist', )
    serializer = StateModuleSerializer(role, data=request.data, partial=True)
    if serializer.is_valid():
        serializer.save()
        return exception_handler.error_handling(data=serializer.data, status_code=200)
    return exception_handler.error_handling(serializer.errors)

@api_view(['POST'])
# @permission_classes([IsAuthenticated])
def save_state(request):
    # add to create by and update by data
    auth_detail = AuthValidation(user_id=request.user.id)
    request.data.update(auth_detail)

    serializer = StateModuleSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return exception_handler.error_handling(data=serializer.data)
    return exception_handler.error_handling(errMsg=serializer.errors)


@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def delete_state(request, role_id):
    try:
        role = State.objects.get(id=role_id)
    except State.DoesNotExist:
        exception_handler.error_handling(errMsg='role id does not exist', )
    role.delete()
    return exception_handler.error_handling(data='role id delete succefully', status_code=200)
