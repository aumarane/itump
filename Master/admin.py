from django.contrib import admin
from Master.models import *


class CountryMasterAdmin(admin.ModelAdmin):
    list_display = ('name','status_id',)


class StateAdmin(admin.ModelAdmin):
    list_display = ('name','country_id','status_id',)


class EntityAdmin(admin.ModelAdmin):
    list_display = ('name', 'status_id',)


admin.site.register(CountryMaster, CountryMasterAdmin)
admin.site.register(State, StateAdmin)
admin.site.register(Entity, EntityAdmin)
