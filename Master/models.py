from django.db import models

# Create your models here.
from Account.models import Status


class CountryMaster(models.Model):
    name = models.CharField(max_length=250)
    status_id = models.ForeignKey(Status, on_delete=models.CASCADE, default=1)

    def __str__(self):
        return self.name


class State(models.Model):
    name = models.CharField(max_length=250)
    country_id = models.ForeignKey(CountryMaster, on_delete=models.CASCADE)
    status_id = models.ForeignKey(Status, on_delete=models.CASCADE, default=1)

    def __str__(self):
        return self.name


class Entity(models.Model):
    name = models.CharField(max_length=250)
    status_id = models.ForeignKey(Status, on_delete=models.CASCADE, default=1)

    def __str__(self):
        return self.name
