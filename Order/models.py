from django.db import models

from Account.models import UserModule, Status
from Master.models import Entity, CountryMaster, State
from Services.models import Services, ServicesAddon


class Quote(models.Model):
    user_id = models.ForeignKey(UserModule, on_delete=models.CASCADE)


# iterable
TYPE_CHOICES = (
    ("logo", "logo"),
    ("business", "business"),
)


class OrderDetails(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    email = models.CharField(max_length=250)
    # email = models.ForeignKey(UserModule, on_delete=models.CASCADE)
    order_status = models.BooleanField(default=True)
    amount = models.FloatField()
    service_id = models.ForeignKey(Services, on_delete=models.CASCADE)
    type = models.CharField(max_length=250, choices=TYPE_CHOICES)


class OrderAddonMapping(models.Model):
    order_id = models.ForeignKey(OrderDetails, on_delete=models.CASCADE)
    addon_id = models.ForeignKey(ServicesAddon, on_delete=models.CASCADE)


class SubscriptionPlan(models.Model):
    name = models.CharField(max_length=250)
    description = models.TextField()
    status_id = models.ForeignKey(Status, on_delete=models.CASCADE, default=1)


class UserSubscriptionPlan(models.Model):
    user_id = models.ForeignKey(UserModule, on_delete=models.CASCADE)
    subscription_plan_id = models.ForeignKey(SubscriptionPlan, on_delete=models.CASCADE)
    status_id = models.ForeignKey(Status, on_delete=models.CASCADE, default=1)


class BusinessRegistrationInfo(models.Model):
    entity_id = models.ForeignKey(Entity, on_delete=models.CASCADE)
    country_id = models.ForeignKey(CountryMaster, on_delete=models.CASCADE)
    status_id = models.ForeignKey(Status, on_delete=models.CASCADE, default=1)
    state_id = models.ForeignKey(State, on_delete=models.CASCADE)
    city = models.CharField(max_length=250)
    zipcode = models.CharField(max_length=250)
    primary_contact_first_name = models.CharField(max_length=250)
    primary_contact_last_name = models.CharField(max_length=250)
    primary_phone_number = models.CharField(max_length=250)
    address = models.CharField(max_length=250)
    address_line_2 = models.CharField(max_length=250)
    # city = models.CharField(max_length=250)
    business_name = models.CharField(max_length=250)
    business_address = models.CharField(max_length=250)
    EIN = models.BooleanField()
    contact_person_name = models.CharField(max_length=250)
    contact_person_add = models.TextField(max_length=300)
    # cont_person_address = models.CharField(max_length=250)
    # contact_person_address = models.CharField(max_length=250, blank=True, null=True)
    contact_person_SSN = models.CharField(max_length=250)
    contact_person_phone_no = models.CharField(max_length=250)
    faster_delivery = models.BooleanField()
    pro_business_pack = models.BooleanField()
    SSN = models.CharField(max_length=250)
    order_id = models.ForeignKey(OrderDetails, on_delete=models.CASCADE, related_name='business')
