from rest_framework import serializers

from .models import *


class OrderDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderDetails
        fields = '__all__'

    def create(self, validated_data):
        return OrderDetails.objects.create(**validated_data)


class BusinessRegistrationInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessRegistrationInfo
        fields = '__all__'


class OrderAddonMappingSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderAddonMapping
        fields = '__all__'
