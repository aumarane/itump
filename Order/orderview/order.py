import json

from django.contrib.postgres.search import SearchVector
from django.db.models import Q, F
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated

from Logo.models import LogoCreationInfo
from Logo.serializers import LogoCreationInfoSerializer
from Order.serializers import *
from ResponseHandle import exception_handler


@api_view(['POST'])
# @permission_classes([IsAuthenticated])
def create_order(request):

    # check input data validations
    if not request.data:
        return exception_handler.error_handling('invalid access')

        # Deserializing json data
    payload = request.data.get('data')
    data = json.loads(payload)

    # converts query dict to original dict
    logo_images = dict((request.data).lists())['logo_image']
    print(logo_images)
    if logo_images:
        logo_image = logo_images[0]
    print(logo_image,"ehfghgfh")
    # create memory to store the request data
    order_detail = data['order_detail']
    business_register = data['business_register']
    logo_creation = data['logo_creation']
    logo_creation['logo_image'] = logo_image


    if not order_detail:
        return exception_handler.error_handling('order detail data is required')

    if not order_detail['type']:
        return exception_handler.error_handling('type is required')

    # initialize the out put object
    output = {}

    # order details update
    order = OrderDetailsSerializer(data=order_detail)

    # validate the order detail
    if order.is_valid():
        order.save()
        output['order_detail'] = order.data
        output['v1'] = 'order details submitted successfully'
    else:
        return exception_handler.error_handling(order.errors)

    # order multiple addon order data have to save
    for addon_obj in order_detail['addon_list']:
        addon_map_ser = OrderAddonMappingSerializer(data={
            "order_id": order.data['id'],
            "addon_id": addon_obj['addon_id']
            })

        # serialization validation
        if addon_map_ser.is_valid():
            addon_map_ser.save()
            output['v2'] = 'addon mapping table submitted successfully'
        else:
            return exception_handler.error_handling(addon_map_ser.errors)

    if order_detail['type'] in 'business':
        # add to order id to business request
        business_register['order_id'] = order.data['id']

        # business register serializer
        business = BusinessRegistrationInfoSerializer(data=business_register)
        if business.is_valid():
            business.save()
            output['v3'] = 'business register submitted successfully'
            output['register_information'] = business.data
        else:
            return exception_handler.error_handling(business.errors)

    if order_detail['type'] in 'logo':
        logo_creation['order_id'] = order.data['id']
        serializer = LogoCreationInfoSerializer(data=logo_creation)
        if serializer.is_valid():
            serializer.save()
            output['logo_details'] = serializer.data
            return exception_handler.error_handling(data=serializer.data)
        else:
            return exception_handler.error_handling(serializer.errors)
    return exception_handler.error_handling(data=output)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_order_history(request):
    search_term = request.query_params.get('search')
    order_by = request.query_params.get('order_by')
    page = request.query_params.get('page')
    qty = request.query_params.get('qty')

    # get user auth
    email = request.user.email

    # get the all data for the order
    order = OrderDetails.objects.filter(
        Q(logo__id__isnull=False)
        | Q(business__id__isnull=False)
        & Q(email=email)
    )
    order_obj = order.values(
        'id',
        'date',
        'email',
        'order_status',
        'amount',
        'service_id',
        service_name=F('service_id__name'),
    )

    # get the logo details
    for logo in order_obj:
        logo_obj = LogoCreationInfo.objects.filter(order_id=logo['id']).values()
        if logo_obj:
            logo['logo_details'] = logo_obj[0]
        else:
            logo['logo_details'] = ''

    # get the logo details
    for buss in order_obj:
        buss_obj = LogoCreationInfo.objects.filter(order_id=buss['id']).values()
        if buss_obj:
            buss['business_details'] = buss_obj[0]
        else:
            buss['business_details'] = ''

    # if search_term:
    #     combined_list = order.annotate(
    #         search=SearchVector('amount'), )
    #     print(combined_list)
    #     output = combined_list.filter(search__icontains='hello')
    #     print(output)
    #     a = output.values()
    #     print(a)
    #     return exception_handler.error_handling(data=a)

    return exception_handler.error_handling(data=order_obj)
