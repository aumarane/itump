from django.urls import path

from Order.orderview import order

urlpatterns = [
    path('create_order', order.create_order),
    path('get_order_history', order.get_order_history),

]
