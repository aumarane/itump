from django.contrib import admin
from Order.models import *

admin.site.register(Quote)
admin.site.register(OrderDetails)
admin.site.register(SubscriptionPlan)
admin.site.register(UserSubscriptionPlan)
admin.site.register(BusinessRegistrationInfo)
